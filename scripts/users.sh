#!/bin/bash

set -m

printf "\n=> Setting auth...\n"

printf "\n=> Start without auth...\n"

mongod --dbpath $MONGO_DB_PATH --nojournal &

printf "\n=> Waiting...\n"

sleep 3

printf "\n=> Create user root...\n"

mongo admin --eval "db.createUser({user: '$MONGO_ROOT_USER',pwd: '$MONGO_ROOT_PASSWORD', roles: [{ role: 'root', db: 'admin' }, 'readWriteAnyDatabase']});"

printf "\n=> Create user app...\n"

mongo $MONGO_APP_DATABASE --eval "db.createUser({user: '$MONGO_APP_USER', pwd: '$MONGO_APP_PASSWORD', roles: [{ role: 'readWrite', db: '$MONGO_APP_DATABASE' }, { role: 'read', db: 'local' }]});"

printf "\n=> Shutdown mongo...\n"

mongod --dbpath $MONGO_DB_PATH --shutdown


