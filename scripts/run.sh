#!/bin/bash

set -m

printf "\n=> Setting mongo...\n"

chown -R mongodb:mongodb $MONGO_DB_PATH

if [ "$MONGO_ROLE" == "primary" ]; then
  $MONGO_SCRIPTS_DIR/users.sh
  $MONGO_SCRIPTS_DIR/replicaset.sh
fi

printf "\n=> Start...\n"

mongod \
  --bind_ip_all \
  --dbpath $MONGO_DB_PATH \
  --logpath $MONGO_LOG_PATH \
  --storageEngine $MONGO_STORAGE_ENGINE \
  --keyFile $MONGO_KEYFILE \
  --replSet $MONGO_REPLICASET \
  --smallfiles \
  --auth &

fg

