FROM mongo:4.0.1

# add default envs
ENV MONGO_SCRIPTS_DIR /opt/mongo/scripts
ENV MONGO_SECRETS_DIR /opt/mongo/secrets
ENV MONGO_KEYFILE /opt/mongo/secrets/keyfile
ENV MONGO_DB_PATH /data/db
ENV MONGO_STORAGE_ENGINE wiredTiger
ENV MONGO_LOG_PATH /var/log/mongodb.log

# copy scripts
COPY scripts $MONGO_SCRIPTS_DIR
RUN chmod +x $MONGO_SCRIPTS_DIR/*

# copy secrets for mongo auth
COPY secrets $MONGO_SECRETS_DIR
RUN chown mongodb:mongodb $MONGO_KEYFILE
RUN chmod 400 $MONGO_KEYFILE

# expose ports
EXPOSE 27017 28017

# set workdir to scripts
WORKDIR $MONGO_SCRIPTS_DIR

CMD ["./run.sh"]
