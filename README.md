# Cluster MongoDB com Docker

Este é um projeto que tem por objetivo, criar de forma simples, rápida e segura, um **Cluster** do **MongoDB** usando **Docker**, mais especificamente usando **Docker Machine**.

Segue uma imagem simplista de como ficará nosso cluster e seu acesso.

![Cluster](img/mongo-replicaset.png?raw=true "Cluster MongoDB")

# Criação das VMs

O principal motivo para se usar o **Docker Machine**, é se aproximando o máximo do ambiente de produção (máquina, rede, firewall etc...). Desta forma, é possível usar este mesmo processo na **AWS** ou qualquer outro provedor, e até trabalhar com uma arquitetura híbrida entre **AWS** + **GCP** + **Azure**.

Vamos criar três VMs, uma para o mongo primário, e outras duas para os secundários.

```
docker-machine create --driver virtualbox mongo1
docker-machine create --driver virtualbox mongo2
docker-machine create --driver virtualbox mongo3
```

Para listar as VMs e seus respectivos IPs:

```
docker-machine ls
```

O resultado deve se parecer como a imagem abaixo:

![Screenshot](img/docker-machine-ls-result.png?raw=true "Resultado")

# Configuração

## Versão do Mongo

Aqui vamos usar a versão 4.0.1 do MongoDB, mas você pode especificar qualquer outra versão na primeira linha do arquivo **Dockerfile**, alterando por exemplo para **FROM mongo:3.6**

## Segurança

Para que as replicas do mongo consigam se comunicar com segurança, é preciso criar uma chave única para o cluster.

```
openssl rand -base64 756 > ./secrets/keyfile
```

## Hostnames e portas de acesso

Para que o serviços se comuniquem corretamente, também é preciso ajustar o hostname e porta de cada VM. Para isso, revise o arquivo **docker-compose.yml**.

Para ter certeza do IP de cada VM, execute os comandos:

```
docker-machine ip mongo1
docker-machine ip mongo2
docker-machine ip mongo3
```

# Iniciando o cluster

Primeiro iniciaresmos os hosts secundários, pois quando iniciarmos o primário todos serão adicionados automaticamente no cluster. Do contrário, sempre teriamos que acessar o primário e adicionar os secundários manualmente.

## Iniciando o "primeiro" mongo secundário

```
eval $(docker-machine env mongo2)
docker-compose up -d mongo2
```

## Iniciando o "segundo" mongo secundário

```
eval $(docker-machine env mongo3)
docker-compose up -d mongo3
```

## Iniciando o mongo primário

Após iniciar o primário e todos os secundários estiverem rodando, todo serão sincronizados, incluindo os acessos dos usuários.

```
eval $(docker-machine env mongo1)
docker-compose up -d mongo1
```

# Primeiro acesso

Agora vamos acessar o mongo primário:

```
docker exec -it mongo1 mongo admin -u root -p root
```

Se tudo der certo, seu console vai mudar para o console do mongo já com replicaset ativo:

```
rs0:PRIMARY>
```

Pode ser que o cluster já tenha elegido outro mongo como primário, e o console também pode se parecer com isso:

```
rs0:SECONDARY>
```

Para ver os status:

```
rs0:PRIMARY> rs.status()
```

Listar apenas os membros do cluster:

```
rs0:PRIMARY> rs.status().members
```

O resultado deve ser uma lista com todos os membros do cluster, seus nomes, status, prioridades, se é primário, secundário etc...

# Dicas

## Para acessar cada mongo

```
eval $(docker-machine env mongo2)
docker exec -it mongo2 mongo admin -u root -p root
```

A primeira linha só é necessária, caso esteja com o ambiente do docker apontando para outra VM.

Caso tenha o client do mongo instalado na sua máquina, você pode conectar diretamente sem depender do docker:

```
mongo --host mongo2 -u root -p root admin
```

## Acessando o replicaset

```
mongo mongodb://appUser:appPassword@mongo1,mongo2,mongo3/appDatabase?replicaSet=rs0
```

Geralmente, esta é a forma que a aplicação vai usar, veja que já estamos usando o usuário, senha e banco da aplicação.

Aqui já podemos realizar operações de CRUD e testar a replicação dos dados e a redundância do mongo.

## Referências:

- [Docker Compose](https://docs.docker.com/compose/overview/)
- [Docker Machine](https://docs.docker.com/machine/overview/)
- [+ Docker](https://www.google.com/search?q=start+with+docker)
- [MongoDB Replicaset](https://docs.mongodb.com/manual/replication/)
- [+ MongoDB](https://www.google.com/search?q=start+with+mongodb)
